Wiim::Application.routes.draw do
  root :to                     => 'home#index'
  get 'brands.json'            => 'brands#index'
  get 'brands/new'             => 'brands#new',        as: 'new_brand'
  post 'brands'                => 'brands#create'
  get 'brands/:id'             => 'brands#show'

  get 'admin'                 => 'admin#index',      as: 'admin_index'
  get 'admin/submissions/:id' => 'admin#submission', as: 'brand_submission'
  get 'admin/submissions'     => 'admin#submissions', as: 'brands_submissions'
  put 'admin/submissions/approve/:id' => 'admin#approve',    as: 'brand_approval'
  put 'admin/submissions/reject/:id' => 'admin#reject',    as: 'brand_rejection'
end
