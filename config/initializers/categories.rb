Wiim::Application.config.categories = 
[
  'Accessories',
  'Clothes',
  'Food',
  'Health & Beauty',
  'Home furniture',
  'Jewellery',
  'Kitchenware',
  'Shoes',
  'Toys',
  'Wine & Spirits'
]
