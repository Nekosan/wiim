class CreateBrands < ActiveRecord::Migration
  def change
    create_table("brands") do |t|
      t.string :name, null: false
      t.string :url
      t.text :description
    end
  end
end
