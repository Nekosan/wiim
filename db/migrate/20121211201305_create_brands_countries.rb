class CreateBrandsCountries < ActiveRecord::Migration
  def change
    create_table(:brands_countries, id: false) do |t|
      t.integer :brand_id
      t.integer :country_id
    end
  end
end
