Tire.index("wiim-#{Rails.env}") do
  delete
  create
end

#seed db with countries
Country.delete_all
Wiim::Application.config.countries.each do |country|
  Country.create!(name: country)
end

#seed db with categories
Category.delete_all
Wiim::Application.config.categories.each do |category|
  Category.create!(name: category)
end
