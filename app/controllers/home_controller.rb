class HomeController < ApplicationController
  def index
    query = params['query'] || '?'
    @approved_brands = Brand.search query
  end
end
