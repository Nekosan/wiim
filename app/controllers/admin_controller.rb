class AdminController < ApplicationController

  before_filter :authenticate

  def index
  end

  def submissions
    @brands = Brand.order(:name)
  end

  def submission
    brand_record = Brand.find(params[:id])
    @brand = BrandsSerializer.make_struct(brand_record)
    @status = Brand.find(params[:id]).status
  end

  def approve
    brand = Brand.find(params[:id])
    brand.approve
    redirect_to brands_submissions_path
  end

  def reject
    brand = Brand.find(params[:id])
    brand.reject
    redirect_to brands_submissions_path
  end

  protected

  def authenticate
    authenticate_or_request_with_http_digest do |username|
      ENV['WIIM_PASS'] if username == ENV['WIIM_USERNAME']
    end
  end
end
