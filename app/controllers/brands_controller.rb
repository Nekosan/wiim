class BrandsController < ApplicationController
  def index
    brands = Brand.approved.includes(:categories, :countries)
    render json: BrandsSerializer.serialize(brands).to_json
  end

  def show
    @brand = Brand.search("id: #{params[:id]}").first
  end

  def new
    @brand = Brand.new
    @countries = Country.all
    @categories = Category.all
  end

  def create
    @brand = BrandBuilder.build params

    if @brand.save
      flash[:notice] = "Thank you for creating a new brand! We will be reviewing it shortly..."
      redirect_to '/'
    else
      flash[:error] = @brand.errors
      render :new
    end
  end
end
