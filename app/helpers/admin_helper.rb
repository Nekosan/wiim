module AdminHelper
  def display_status(status)
    case status
    when 'approved'; 'success'
    when 'pending'; 'info'
    when 'rejected'; 'error'
    end
  end
end
