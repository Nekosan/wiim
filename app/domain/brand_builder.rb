module BrandBuilder
  def self.build params
    brand = Brand.new params[:brand]

    [:categories, :countries].each do |attr|
      add_attribute_to_brand brand, attr, params
    end
    
    brand
  end

  private

  def self.add_attribute_to_brand brand, attribute, params
    attribute_names = params[attribute] || []
    attribute_value = attr_class(attribute).where(name: attribute_names)
    #brand.countries = countries
    brand.send("#{attribute}=".to_sym, attribute_value)
  end

  #:countries => Country
  def self.attr_class attribute
    attribute.to_s.singularize.capitalize.constantize
  end
end
