class Category < ActiveRecord::Base
  validates_presence_of :name
  validates_uniqueness_of :name
  validates :name, inclusion: {in: Wiim::Application.config.categories}

  attr_accessible :name

  has_and_belongs_to_many :brands
end
