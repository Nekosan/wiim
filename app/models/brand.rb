class Brand < ActiveRecord::Base
  validates_associated :countries
  validates_associated :categories
  validates_presence_of :countries
  validates_presence_of :categories
  validates_presence_of :name
  validates_uniqueness_of :name
  validates :status, inclusion: { in: %w(approved pending rejected) }
  validates :url, format: { with: /\Ahttp(s?):\/\/|\A\z/ }

  attr_accessible :name, :url, :description, :countries, :categories

  has_and_belongs_to_many :countries
  has_and_belongs_to_many :categories

  include Tire::Model::Search

  index_name "wiim-#{Rails.env}"

  def to_indexed_json
    BrandsSerializer.serialize(self).to_json
  end

  after_save do
    if status == 'approved'
      update_index 
    else
      Brand.index.remove self
    end
  end

  def approve
    self.status = 'approved'
    save!
  end

  def reject
    self.status = 'rejected'
    save!
  end

  def self.approved
    where(status: 'approved')
  end
end
