class BrandsSerializer
  def self.serialize(arg)
    if arg.is_a?(Brand)
      brand = arg
      {
        'id'          => brand.id,
        'description' => brand.description,
        'name'        => brand.name,
        'url'         => brand.url,
        'countries'   => brand.countries.map(&:name),
        'categories'  => brand.categories.map(&:name)
      }
    else
      brands = arg
      brands.map{ |b| serialize(b) }
    end
  end

  def self.make_struct(brand)
    OpenStruct.new serialize(brand)
  end
end
