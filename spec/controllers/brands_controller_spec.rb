require 'spec_helper'

describe BrandsController do
  let(:brand) { Brand.new }

  describe 'POST create' do

    before do
      BrandBuilder.stub(:build).and_return brand
    end

    context 'when the new brand is valid' do
      before do
        brand.stub(:save).and_return brand
      end

      it 'redirects to home' do
        post :create
        page.should redirect_to '/'
      end
    end

    context 'when the new brand is invalid' do
      before do
        brand.stub(:save).and_return false
      end

      it 'stays on the same page' do
        post :create
        page.should render_template :new
      end
    end
  end
end
