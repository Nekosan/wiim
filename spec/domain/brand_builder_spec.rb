describe BrandBuilder do
  describe '#build' do
    it 'creates a brand with basic info from params' do
      country1  = Country.create!(name: 'France')
      category1 = Category.create!(name: 'Clothes')

      params = {
        brand: {
          name: 'name',
          url:  'url',
          description: 'description'
        },
        countries:  [ country1.name ],
        categories:  [ category1.name ]
      }

      brand = BrandBuilder.build params

      brand.name.should       == 'name'
      brand.url.should        == 'url'
      brand.countries.should  == [country1]
      brand.categories.should == [category1]
    end
  end
end
