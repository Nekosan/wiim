require 'spec_helper'

describe BrandsSerializer do
  let(:brand) {
    brand = Brand.create!({ description: 'description',
                            name: 'name',
                            url: 'http://example.com',
                            countries: [Country.find_or_create_by_name(name: 'France')],
                            categories: [Category.find_or_create_by_name(name: 'Food')]
    })
  }
  describe '.serialize' do
    context 'when given a brand' do
      it 'serializes it in the right format' do
        serialized_brand = BrandsSerializer.serialize(brand)

        serialized_brand.should == {
          'id' => brand.id,
          'description' => 'description',
          'name' => 'name',
          'url' => 'http://example.com',
          'countries' => ['France'],
          'categories' => ['Food']
        }
      end
    end

    context 'when given multiple brands' do
      it 'serializes them in the right format' do
        serialized_brands = BrandsSerializer.serialize([brand, brand])

        serialized_brands.first.should == {
          'id' => brand.id,
          'description' => 'description',
          'name' => 'name',
          'url' => 'http://example.com',
          'countries' => ['France'],
          'categories' => ['Food']
        }

        serialized_brands.first.should == serialized_brands.second
      end
    end
  end
end
