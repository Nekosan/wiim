Feature: Get brands in JSON

Background:
	Scenario: get brands in JSON
    Given the brand "NewBrand"
    When I want to get the brands information in JSON
    Then I should receive a JSON output with the brand NewBrand
