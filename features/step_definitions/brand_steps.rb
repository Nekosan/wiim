#Scenario: submit a brand
Given /^the country "Japan"$/ do
  japan
end

Given /^the category "Food"$/ do
  food
end

When /^I submit a new brand Muji$/ do
  visit '/brands/new'

  fill_in 'Name',        with: 'Muji'
  fill_in 'Website',     with: "http://www.muji.com"
  fill_in 'description-field', with: "Japanese stores"
  country_input = find '#country-chooser input'
  country_input.set 'Japan'
  country_input.native.send_keys(:return)
  category_input = find '#category-chooser input'
  category_input.set 'Food'
  category_input.native.send_keys(:return)

  click_button 'Submit'
end

Then /^I should be redirected to the home page$/ do
  current_path.should == '/'
end

Then /^I should see a thanks message$/ do
  page.should have_text 'Thank you'
end

#Scenario: approve a brand

Given /^there is a submission for NewBrand$/ do
  newbrand
end

Given /^I am logged in as admin$/ do
  class AdminController < ApplicationController 
    def authenticate
      true
    end
  end
end

When /^I approve NewBrand$/ do
  visit '/admin/submissions'
  click_link 'NewBrand'
  click_button "Approve"
end

Then /^NewBrand should be approved$/ do
  page.should have_text 'approved'
end

#Scenario: search a brand

Given /^a brand "(\w+)" with a country "(.*)"$/ do |brand_name, country_name|
  country = case country_name
    when 'United States'; us
    when 'Japan'; japan
    end

  attributes = { name: brand_name, countries: [country], categories: [food]}
  brand = Brand.create! attributes
  brand.status = 'approved'
  brand.save!
end

When /^I search for "japan"$/ do
  visit '/'
  Brand.tire.index.refresh
  fill_in 'search-form', with: 'japan'
  click_button 'Search'
end

Then /^I should see the brand Muji$/ do
  page.should have_text 'Muji'
end

Then /^I should not see the brand NYD$/ do
  page.should have_no_text "NYD"
end

#Scenario: Get brands in JSON

Given /^the brand "NewBrand"$/ do
  newbrand_approved
end

When /^I want to get the brands information in JSON$/ do
  visit '/brands.json'
end

Then /^I should receive a JSON output with the brand NewBrand$/ do
  page.should have_text("NewBrand")
end
