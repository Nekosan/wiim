Feature: search a brand

  Scenario: search a brand
    Given a brand "Muji" with a country "Japan"
    And a brand "NYD" with a country "United States"
    When I search for "japan"
    Then I should see the brand Muji
    And I should not see the brand NYD
