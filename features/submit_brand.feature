Feature: New brand submission

Background:
  Given the country "Japan"
  And the category "Food"

@javascript
Scenario: submit a new brand
  When I submit a new brand Muji
  Then I should be redirected to the home page
  And I should see a thanks message
