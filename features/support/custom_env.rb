def newbrand
  Brand.find_by_name('NewBrand') || create_new_brand
end

def newbrand_approved
  brand = create_new_brand
  brand.update_attribute :status, 'approved'
end

def us
  Country.find_or_create_by_name name: "United States"
end

def japan
  Country.find_or_create_by_name name: "Japan"
end

def food
  Category.find_or_create_by_name name: "Food"
end

private
def create_new_brand
  Brand.create!(name: 'NewBrand', countries: [japan], categories: [food])
end
