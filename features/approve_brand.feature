Feature: New brand approval

Scenario: approve a brand
  Given there is a submission for NewBrand
  And I am logged in as admin
  When I approve NewBrand
  Then NewBrand should be approved
